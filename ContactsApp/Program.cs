﻿using System;
using System.Collections.Generic;

namespace ContactsApp
{
    class Program
    {
        static void Main(string[] args)
        {
            // Initializes a contact list with preset contacts
            ContactList contactList = new ContactList();
            contactList.InitContacts();
            
            // Outputs the contacts to the user for ease of use
            Console.WriteLine("Registered contacts are: ");
            contactList.printAllContacts();
            
            // Sets active state to let the user search more than once
            int active = 1;

            while(active == 1)
            {
                // retrieves the search phrase, trims white space and sets to lowercase
                Console.WriteLine("Hello, please type a search phrase to search for a contact and hit enter");
                string searchPhrase = Console.ReadLine().Trim().ToLower();

                // loops through the contacts set and adds each match to the results set
                ContactList result = contactList.FindContacts(searchPhrase);

                // Outputs the result if result is greater than 0
                if(result.contacts.Count > 0)
                {
                    Console.WriteLine("\nWe found " + result.contacts.Count + " contact(s) matching the search phrase.");
                    Console.WriteLine("Is this any of the contact(s) you were looking for? \n");

                    result.printAllContacts();
                }
                // if not print sorry statement
                else
                {
                    Console.WriteLine("Couldn't find any contacts with that search phrase, please try again.\n");
                }
                
                // prompt the user if they want to continuo, exits loop if 1 is not entered
                Console.WriteLine("\nWould you like to continue looking for contacts? (type 1 to continue, 0 to exit) \n");
                
                try
                {
                    active = int.Parse(Console.ReadLine());
                } 
                catch (Exception e)
                {
                    // Exits the application if incorrect type of input
                    Console.WriteLine("\n\nHi, probably couldn't parse your input, sos, exiting program...\n\n");
                    active = 0;
                } 
            }
        }

        

        

        
    }

    // Contact class to hold firstname and lastname values
    class Contact
    {
        public string firstName;
        public string lastName;

        public Contact(string firstName, string lastName)
        {
            this.firstName = firstName;
            this.lastName = lastName;
        }

        // Prints the contact's info
        public void Print()
        {
            Console.WriteLine("First name: " + this.firstName + " last name : " + this.lastName);
        }
    }

    class ContactList
    {
        public HashSet<Contact> contacts = new HashSet<Contact>();

        // initialises the contact list with a preset of values
        public void InitContacts()
        {
            contacts.Add(new Contact("Regine", "Urtegård"));
            contacts.Add(new Contact("Reginald", "Urtehouse"));
            contacts.Add(new Contact("Reinhardt", "Giskegård"));
            contacts.Add(new Contact("Robert", "Giske"));
            contacts.Add(new Contact("Jostein", "Fjellheim"));
            contacts.Add(new Contact("Sigurd", "Nedregård"));
        }

        // Locates a contact based on a search param
        public ContactList FindContacts(string searchParam)
        {
            // a hashset that stores the results of the search phrase
            ContactList results = new ContactList();

            // loops through the contacts set and adds each match to the results set
            foreach (Contact contact in this.contacts)
            {
                // Checks if first or last name contains the search phrase (everything is in lowercase)
                if (contact.firstName.ToLower().Contains(searchParam) || contact.lastName.ToLower().Contains(searchParam))
                {
                    // if true then add it to results
                    results.contacts.Add(contact);
                }
            }
            return results;
        }

        // Prints all contacts
        public void printAllContacts()
        {
            foreach (Contact contact in this.contacts)
            {
                contact.Print();
            }
        }
    }

    
}

